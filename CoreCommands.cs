using System.Diagnostics;
using System.Text;

namespace Core
{
    public static class CoreCommands
    {
        private static Process CreateProcess(string commandName, IEnumerable<string> paramsList, bool output = false)
        {
            string paramString = paramsList.Aggregate<string, string>(null,
                (current, param) => current + " " + param);
            return new Process
            {
                StartInfo =
                    {
                        FileName = commandName, // полный путь до инстансыруемого приложения
                        Arguments = paramString, // сюда передаются аргументы командной строки
                        UseShellExecute = output ? !output : !true, // Параметр отвечает за фоновое выполнение
                        RedirectStandardOutput = output // Параметр отвечает за переадресацию
                    }
            };
        }
        public static string ExecShellCommandAsync(string commandName, IEnumerable<string> paramsList = null)
        {
            var bufer = new StringBuilder();
            using (var proc = CreateProcess(commandName, paramsList, true))
            {
                proc.Start(); // инстансыруемся
                while (!proc.StandardOutput.EndOfStream)
                {
                    bufer.AppendLine(proc.StandardOutput.ReadLine()); // пишем в буфер
                }
            }
            return bufer.ToString();
        }
        public static string ExecShellCommand(string command, string arguments = "")
        {
            var processStartInfo =
                arguments != "" ? new ProcessStartInfo(command, arguments) : new ProcessStartInfo(command);

            // переводим программу в CLI режим.
            processStartInfo.UseShellExecute = false;

            // разрешаем читать вывод процесса.
            processStartInfo.RedirectStandardOutput = true;

            var process = new Process();
            process.StartInfo = processStartInfo;

            process.Start();
            var output = process.StandardOutput.ReadToEnd();

            return output;
        }

        public static void CopyDir(string FromDir, string ToDir)
        {
            Directory.CreateDirectory(ToDir);
            foreach (string s1 in Directory.GetFiles(FromDir))
            {
                Console.WriteLine(s1);
                string s2 = ToDir + "//" + Path.GetFileName(s1);
                File.Copy(s1, s2, true);
            }
            foreach (string s in Directory.GetDirectories(FromDir))
            {
                Console.WriteLine(s);
                CopyDir(s, ToDir + "//" + Path.GetFileName(s));
            }
        }
    }
}