﻿using Core;
using USB;
namespace Rufus
{
    class Init
    {
        static void Main(string[] args)
        {
            WriteISONTFS("/home/fast/Документы/ru-ru_windows_10_consumer_editions_version_21h2_updated_april_2022_x64_dvd_0dd54aad.iso", "/dev/sdb");
        }
        private static void WriteISONTFS(string ISOPath, string device)
        {
            USBDevice.UnmountDevice(device);
            USBDevice.UnmountDevice(ISOPath);
            CoreCommands.ExecShellCommand("wipefs", $"--all {device}");
            CoreCommands.ExecShellCommand("parted", $"--script {device} mklabel msdos");
            CoreCommands.ExecShellCommand("parted", $"--align none --script {device} mkpart primary fat32 -- -2128s -1s");
            CoreCommands.ExecShellCommand("mkfs.vfat", $"-F 32 -I {device}1");
            CoreCommands.ExecShellCommand("parted", $"--script {device} mkpart primary ntfs 4MiB 100%");
            CoreCommands.ExecShellCommand("mkfs.ntfs", $"--quick --label \"Windows\" {device}2");
            File.Copy($"{Environment.CurrentDirectory}/uefi-ntfs.img", $"{device}1", true);
            USBDevice.MountDevice(ISOPath, "/mnt/iso");
            USBDevice.MountDevice($"{device}2", "/mnt/flash");
            CoreCommands.CopyDir("/mnt/iso", "/mnt/flash");
            CoreCommands.ExecShellCommand("ms-sys", $"-7 -f {device}");
            CoreCommands.ExecShellCommand("ms-sys", $"-r -n {device}2");
        }
    }
}